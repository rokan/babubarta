package com.example.project;

import nogsantos.ufg.br.qrscanner.ScanActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class AttendenceFragment extends Fragment {
	
	View view;
	Activity activity;

	Button btnScan;
	Button btnListView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
		
		view = inflater.inflate(R.layout.activity_attendence_fragment, container, false);
		activity = getActivity();
		
		
		btnScan =  (Button) view.findViewById(R.id.btnScan);
		btnScan.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent intent = new  Intent(activity, ScanActivity.class);
				startActivity(intent);
				
				
			}
		});
		
		
		
		btnListView = (Button) view.findViewById(R.id.btnList);
		btnListView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
//				Intent intent = new Intent(activity, AttendanceListFragment.class);
//				startActivity(intent);
//				
				
				
				
				
				Fragment frag = new AttendanceListFragment();
             
				FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(((ViewGroup) getView().getParent()).getId(), frag);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                ft.commit();
				
				
				
				
				
				
				
				
			}
		});
		
		
		return view;
	}

	
}
