package com.example.project;

import static com.roscopeco.ormdroid.Query.eql;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import com.humacLab.animation.CalculateAge;
import com.humacLab.project.sqlite.Members;
import com.roscopeco.ormdroid.Entity;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewActivity extends Activity  {

	
//	View view;
//	Activity activity;
//	
//	MembersViewAdapter viewAdapter;
//	ListView lstView;
//	String [] allmembers = {"Member1","Member2","Member3","Member4","Member5"}; 
//	List<Members> infoMember;
	

	
 private ImageView btnEdit, btnViewImage, imageTransfar;
 private ImageView btnPrevious;
 private TextView viewName, vNametransfar,vTransferDOB, vPragCondition, vPresentPragcondition, vLMP, vAge;
 private TextView viewGuardienName;
 private TextView idAddress, vFamilyIncome, vClubName, vBookNo;
 private TextView vGurdianRelation,vReligious,vTribe,vOccu, vFamilyMem,vEduQuali,vChildNumber, vFirstChild,vSecondChild,vThirdChild, vGuardEduQuali, vGurdOcc,vFamilyHead, 
                  vWaterSource, vToilet,vJoin,vJoiningPermission, vBlood;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewlay);
		
		
		
		//btnEdit = (ImageView) findViewById(R.id.vBtnEdit);		

//		btnEdit.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(ViewActivity.this, EditActivity.class);
//				startActivity(intent);
//				
//			}
//		});
//		
		btnPrevious = (ImageView) findViewById(R.id.vBtnPrev);
		
		btnPrevious.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intt = new Intent(ViewActivity.this, GroupDetailTabsActivity.class);
				startActivity(intt);
				
			}
		});
		
		imageTransfar = (ImageView) findViewById(R.id.imageTransfar);
		
		vAge = (TextView) findViewById(R.id.vAge);
		vLMP = (TextView) findViewById(R.id.vLMP);
		vPragCondition = (TextView) findViewById(R.id.vPragCondition);
		vPresentPragcondition = (TextView) findViewById(R.id.vPresenPragnantCon);
		
		btnViewImage = (ImageView) findViewById(R.id.viewImage);
		vTransferDOB = (TextView) findViewById(R.id.vTransferDOB);
		
		vNametransfar = (TextView) findViewById(R.id.vNametransfar);
		viewName = (TextView) findViewById(R.id.viewName);
		viewGuardienName =  (TextView) findViewById(R.id.viewGuardienName);
		idAddress = (TextView) findViewById(R.id.idAddress);
		
		//vGurdianRelation = (TextView) findViewById(R.id.viewGuardienName);
		 vFamilyIncome = (TextView) findViewById(R.id.vFamilyIncome);
		
		vReligious = (TextView) findViewById(R.id.vReligious);
		vTribe = (TextView) findViewById(R.id.vTribe);
		vOccu = (TextView) findViewById(R.id.vOccupa);
		vFamilyMem = (TextView) findViewById(R.id.vFamilyMember);
		vEduQuali = (TextView) findViewById(R.id.vEducationalQualification);
		vChildNumber = (TextView) findViewById(R.id.vChildNumber);
		vFirstChild = (TextView) findViewById(R.id.vFirstChild);
		vSecondChild = (TextView) findViewById(R.id.vSecondChild);
		vThirdChild = (TextView) findViewById(R.id.vThirdChild);
		vGuardEduQuali = (TextView) findViewById(R.id.vGuardienQali);
		vGurdOcc = (TextView) findViewById(R.id.vGuardienOcc);
		vFamilyHead = (TextView) findViewById(R.id.vFamilyHead);
		vWaterSource = (TextView) findViewById(R.id.vWaterSource);
		vToilet = (TextView) findViewById(R.id.vToilet);
		
		vJoin = (TextView) findViewById(R.id.vJoining);
		vJoiningPermission = (TextView) findViewById(R.id.vJoinnigPermision);
		
		
		vClubName = (TextView) findViewById(R.id.vClubName);
		vBookNo = (TextView) findViewById(R.id.vBookNo);
		vBlood = (TextView) findViewById(R.id.vBloodgroup);
		
		
		 int id = 0;
		 Bundle bun = getIntent().getExtras();
		 try{
			 id = bun.getInt("Members Id");
		 }catch(Exception e){}
		 
		 Members members = Entity.query(Members.class).where(eql("_id", id)).execute();
		 
		 vNametransfar.setText(members.name_en);
		 vTransferDOB.setText(members.dob);
		 viewName.setText(members.name_en);
		 viewGuardienName.setText(members.guardian_name);
		 idAddress.setText(members.address);
		 
        
		 String filePath = members.getPhoto();
		 File file = new File(filePath);
		 Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
		 btnViewImage.setImageBitmap(bitmap);
	     imageTransfar.setImageBitmap(bitmap);	
  
		 
		 vBookNo.setText(Integer.toString(members.book_no));
		 vFamilyIncome.setText(Integer.toString(members.family_income));
		 vClubName.setText(members.club_name);
		 
		 vPragCondition.setText(members.preg_cond);
		 vPresentPragcondition.setText(members.preg_cond);
		 vLMP.setText(members.lmp);
		 
		 
//		 
//         CalculateAge calcu = new CalculateAge();
//		 
 
//		 vAge.setText(calcu.calculateYear());
//		 
		

		 if(members.join!=null)
		 {
		 vJoin.setText(members.join);
		 }
		 else {
			vJoiningPermission.setText("<none>");
		} 
		
		 
		
		 if(members.join_permission!=null)
		 {
		 vJoiningPermission.setText(members.join_permission);
		 }
		 else {
			vJoiningPermission.setText("<none>");
		} 
		
		 
		 
		 
		 
		 
		 if(members.ageOne!=null)
		 {
		 vFirstChild.setText(members.ageOne);
		 }
		 else {
			vFirstChild.setText("<none>");
		} 
		
		
		 

		 if(members.ageTwo!=null)
		 {
		 vSecondChild.setText(members.ageTwo);
		 }
		 else {
			vSecondChild.setText("<none>");
		} 
		
		

		 if(members.ageThree!=null)
		 {
		 vThirdChild.setText(members.ageThree);
		 }
		 else {
			vThirdChild.setText("<none>");
		} 
		
		
		 
		 
		 
		 
		 if(members.blood_group!=null)
		 {
		 vBlood.setText(members.blood_group);
		 }
		 else {
			vBlood.setText("<none>");
		}
		 
		 
		 
		 
		 if(members.religion!=null)
		 {
		 vReligious.setText(members.religion);
		 }
		 else {
			vReligious.setText("<none>");
		}
		 
		 
		 
		 if(members.tribe!=null)
		 {
		 vTribe.setText(members.tribe);
		 }
		 else {
			vTribe.setText("<none>");
		}
		 
		 
		 if(members.occupation!=null)
		 {
		 vOccu.setText(members.occupation);
		 }
		 else {
			vOccu.setText("<none>");
		}
		 
		 
		 
		 
		 if(Integer.toString(members.family_members)!=null)
		 {
		 vFamilyMem.setText(Integer.toString(members.family_members));
		 }
		 else {
			vFamilyMem.setText("<none>");
		}
		 
		 
		 
		 if(members.qualification!=null)
		 {
		 vEduQuali.setText(members.qualification);
		 }
		 else {
			vEduQuali.setText("<none>");
		}
		 
		 
		 if(Integer.toString(members.children)!=null)
		 {
		 vChildNumber.setText(Integer.toString(members.children));
		 }
		 else {
			vChildNumber.setText("<none>");
		}
		  
		 
		 
		 
		 
		 if(members.guardian_qual!=null)
		 {
		 vGuardEduQuali.setText(members.guardian_qual);
		 }
		 else {
			vGuardEduQuali.setText("<none>");
		}
		 
		 
		 
		 
		 
		 
		 if(members.guardian_occ!=null)
		 {
		 vGurdOcc.setText(members.guardian_occ);
		 }
		 else {
			vGurdOcc.setText("<none>");
		}
		 
		 
		 
		 
		 
		 if(members.head!=null)
		 {
		 vFamilyHead.setText(members.head);
		 }
		 else {
			vFamilyHead.setText("<none>");
		}
		 
		 
		 
		 
		 
		 if(members.water_source!=null)
		 {
		 vWaterSource.setText(members.water_source);
		 }
		 else {
			vWaterSource.setText("<none>");
		}
		 
		 
		 
		 
		 if(members.sanitation!=null)
		 {
		 vToilet.setText(members.sanitation);
		 }
		 else {
			vToilet.setText("<none>");
		}
		 
		 
	 
	
		 
		 
		 
		  
			
			
		 
		 
		 
		 
		 
		 
	}



	
	
	
	
		
	
	}

	

	

