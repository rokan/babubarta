package com.example.project;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ListView;

import com.example.projectAdapters.CustomeAdapter;

public class GroupsActivity extends Activity {

	ListView gView;
	Context context;
	ArrayList groupName;
	
	public static String [] groupLeaderList = {"পদ্মা", "ডাকাতিয়া", "মেঘনা", "হালদা ","শাপলা","কমল","শিউলি","করবী","কাঞ্চন"};
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_groups);
		
		gView =(ListView) findViewById(R.id.gridView1);		
		gView.setAdapter(new CustomeAdapter(this, groupLeaderList));
		
        groupName = new ArrayList();
        for (int i = 0; i < groupLeaderList.length; ++i) {
            groupName.add(groupLeaderList[i]);
        }
		
		
	}

	

	
}
