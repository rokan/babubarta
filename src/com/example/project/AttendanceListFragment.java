package com.example.project;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager.LayoutParams;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.projectAdapters.AttendanceListAdapter;
import com.humacLab.project.sqlite.Members;
import com.roscopeco.ormdroid.Entity;

public class AttendanceListFragment extends Fragment {

	private static final int CAMERA_REQUEST = 1888;
	public File myImage;
	public File storagePath;
	private Button btnAttendSave, btnAttendCam;
	View view;
	ListView attendMembers;
	ImageView imageMeeting;
	Context context;
	Activity activity;
	LayoutInflater inflater;
	int id;

	AttendanceListAdapter adapter;

	public static String[] memberAttendancet = { "Group 1", "Group 2",
			"Group 3", "Group 4", "Group 5", "Group 6", "Group 7", "Group 8",
			"Group 9" };

	// @Override
	// protected void onCreate(Bundle savedInstanceState) {
	// super.onCreate(savedInstanceState);
	// setContentView(R.layout.activity_attendance_list);
	//
	// //inflater = (LayoutInflater)
	// context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	// //view = inflater.inflate(R.layout.activity_attendance_list, null);
	// attendMembers = (ListView) findViewById(R.id.lstAttendance);
	// List<Members> attendance_list =
	// Entity.query(Members.class).executeMulti();
	// adapter = new AttendanceListAdapter(this,R.layout.listitem_attandence,
	// attendance_list );
	// attendMembers.setAdapter(adapter);
	//

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		view = inflater.inflate(R.layout.activity_attendance_list, container,
				false);

		btnAttendSave = (Button) view.findViewById(R.id.btnAttendSave);
		attendMembers = (ListView) view.findViewById(R.id.lstAttendance);

		List<Members> attendance_list = Entity.query(Members.class)
				.executeMulti();
		activity = getActivity();

		try {
			id = getArguments().getInt("memberId");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		adapter = new AttendanceListAdapter(getActivity(),
				R.layout.listitem_attandence, attendance_list, id);
		attendMembers.setAdapter(adapter);

		setListViewHeightBasedOnChildren(attendMembers);

		imageMeeting = (ImageView) view.findViewById(R.id.imgMeeting);
		btnAttendCam = (Button) view.findViewById(R.id.btnAttendCam);

		GroupDetailTabsActivity frgmentContainerActivity = (GroupDetailTabsActivity) getActivity();

		// id = frgmentContainerActivity.getID();
		//
		// int b = id;

		btnAttendCam.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent cameraIntent = new Intent(
						android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(cameraIntent, CAMERA_REQUEST);
				imageMeeting.setVisibility(View.VISIBLE);
			}
		});

		btnAttendSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(activity,
						GroupDetailTabsActivity.class);
				startActivity(intent);

				Toast.makeText(activity, "Attendance Saved Successfully!!",
						Toast.LENGTH_SHORT).show();
			}
		});

		return view;

	}

	private void setListViewHeightBasedOnChildren(ListView listView) {

		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;

		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);

			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
						LayoutParams.MATCH_PARENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();

		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ ((listView.getDividerHeight()) * (listAdapter.getCount()));

		listView.setLayoutParams(params);
		listView.requestLayout();

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAMERA_REQUEST && resultCode == activity.RESULT_OK) {
			Bitmap imageBit = (Bitmap) data.getExtras().get("data");
			storagePath = new File(Environment.getExternalStorageDirectory()
					+ "/DCIM/HumacLab/");
			storagePath.mkdirs();
			myImage = new File(storagePath, System.currentTimeMillis() + ".jpg");

			try {
				FileOutputStream out = new FileOutputStream(myImage);
				imageBit.compress(Bitmap.CompressFormat.JPEG, 80, out);
				Uri outputFileUri = Uri.fromFile(myImage);

				imageMeeting.setImageBitmap(imageBit);
				out.close();
				// Toast.makeText(Review.this, "Image saved: " +
				// outputFileUri.toString(), Toast.LENGTH_LONG).show();

			} catch (Exception e) {

			}

		}
	}

}
