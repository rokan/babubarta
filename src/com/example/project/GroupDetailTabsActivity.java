package com.example.project;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class GroupDetailTabsActivity extends FragmentActivity implements
		OnClickListener {

	private FragmentTabHost mTabHost;
	private TextView txtGroupHeader;
	private ImageView btnLogOut, imgCheck, imgUnCheck;
	private int position;
	int id;

	public int getID() {
		return id;
	}

	AttendanceListFragment attand;

	public static String[] groupLeaderList = { "পদ্মা", "ডাকাতিয়া", "মেঘনা",
			"হালদা ", "শাপলা", "কমল", "শিউলি", "করবী", "কাঞ্চন" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_group_detail_tabs);

		imgCheck = (ImageView) findViewById(R.id.imgCheck);
		imgUnCheck = (ImageView) findViewById(R.id.imgUnCheck);

		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

		mTabHost.addTab(mTabHost.newTabSpec("Members").setIndicator("",getResources().getDrawable(R.drawable.ic_action_group)),MembersListFragment.class, null);
		//mTabHost.addTab(mTabHost.newTabSpec("Calendar").setIndicator("",getResources().getDrawable(R.drawable.ic_action_calender)),CalendarFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("Attendance").setIndicator("",getResources().getDrawable(R.drawable.ic_action_list)),AttendenceFragment.class, null);

		// mTabHost.addTab(mTabHost.newTabSpec("Attendance_List").setIndicator("",getResources().getDrawable(R.drawable.ic_action_list)),AttendanceListFragment.class,
		// null);
		// mTabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#1980D6"));
		// mTabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor("#1980D6"));
		// mTabHost.getTabWidget().getChildAt(2).setBackgroundColor(Color.parseColor("#1980D6"));

		// int id=getIntent.getIntExtra("id",/*defaltvalue*/ 2);

		Bundle bundle = getIntent().getExtras();

		if (bundle != null) {
			id = bundle.getInt("Member_Attendance");
			// String formActivity = getIntent().getExtras().getString(
			// "fromResultActivity");

			String formActivity = getIntent().getStringExtra(
					"fromResultActivity");
			if (formActivity != null) {
				if (formActivity.equals("ResultActivity")) {
					Fragment frag = new AttendanceListFragment();
					Bundle fragBundle = new Bundle();
					bundle.putInt("memberId", id);
					// set Fragmentclass Arguments
					frag.setArguments(bundle);

					FragmentManager fm = getSupportFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					ft.replace(android.R.id.tabhost, frag);
					ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					ft.addToBackStack(null);
					ft.commit();
				}
			}

		}

		// int acceptedId = getIntent().getIntExtra("id", 1);
		//
		//
		// for (int i = 0; i < attand.attendMembers.getCount(); i++)
		// {
		// if (i == acceptedId)
		//
		// imgCheck.setSelected(true);
		// }

		// FragmentTransaction ft = getFragmentManager().beginTransaction();
		// ft.replace(((ViewGroup) getView().getParent()).getId(), frag);
		// ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		// ft.addToBackStack(null);
		// ft.commit();

		txtGroupHeader = (TextView) findViewById(R.id.txtGroupHeader);
		txtGroupHeader.setText(groupLeaderList[position]);

		btnLogOut = (ImageView) findViewById(R.id.btnLogOut);
		btnLogOut.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnLogOut:

			Intent intent = new Intent(this, SignInActivity.class);
			startActivity(intent);

			break;

		default:
			break;
		}

	}

}
