package com.example.project;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.humacLab.animation.CalculateAge;
import com.humacLab.project.sqlite.Members;
public class AddFormActivity extends Activity implements OnClickListener {
	
	private static final int CAMERA_REQUEST = 1888;
	
	
	private ImageView imageAdd, addBtnPrev, imageDOB, imageLMP, imagePresentPragnancy;
	private Button btnSave;
	
	 Context context;
	 private int startYear = 2000;
	 private int startMonth = 6;
	 private int startDay = 1;
	 
	 private int startYearLMP = 2015;
	 private int startMonthLMP = 8;
	 private int startDayLMP = 15;
	 
	 static final int DATE_START_DIALOG_ID = 0;
	 static final int PRAGNANT_DATE_START_DIALOG_ID = 2000;
	 CalculateAge age = new CalculateAge();
	
	 Calendar calendar= Calendar.getInstance();
	 SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM");
		
	 
	//private Calendar 
	private EditText etNameBn, etNameEn, etGuardianName,etAddress,etFamilyIncome,etClubName,etBookNo;
	private Spinner spnGuardianRelation, spnReligion, spnBloodGroup,spnTribe,spnOccupation, spnFamilyMember, spnQualification,spnChildren, spnAgeOne,spnAgeTwo,spnAgeThree,
	spnGuardianQuali, spnGuardianOCC, spnHead, spnWaterSource, spnSanitation,spnJoin, spnJoiningPermission;	
	private TextView txtDOB, txtLMP, txtPresentPragnancy;
	public File myImage;
	public File storagePath;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_form);

		
		
		
		
		
		imageAdd =(ImageView) findViewById(R.id.imageAdd);
		imageDOB = (ImageView) findViewById(R.id.imgDOB);
		imageLMP = (ImageView) findViewById(R.id.imgLMP);
		//imagePresentPragnancy = (ImageView) findViewById(R.id.imgPresentPragnancy);
		
		//etNameBn = (EditText) findViewById(R.id.etNameBn);
		etNameEn = (EditText) findViewById(R.id.etNameEn);
		etGuardianName = (EditText) findViewById(R.id.etGuardianName);
		etAddress = (EditText) findViewById(R.id.etAddress);
		etFamilyIncome = (EditText) findViewById(R.id.etFamilyIncome);
		etClubName = (EditText) findViewById(R.id.etClubName);
		etBookNo = (EditText) findViewById(R.id.etBookNo);
		
		txtDOB = (TextView) findViewById(R.id.txtDOB);
		txtLMP = (TextView) findViewById(R.id.txtLMP);
		txtPresentPragnancy = (TextView) findViewById(R.id.txtPresentPragnancy);
		

		spnGuardianRelation = (Spinner) findViewById(R.id.spnGuardianRelation);
		spnReligion = (Spinner) findViewById(R.id.spnReligion);
		spnBloodGroup = (Spinner) findViewById(R.id.spnBloodGroup);
		spnTribe = (Spinner) findViewById(R.id.spnTribe);
		spnOccupation = (Spinner) findViewById(R.id.spnOccupation);
		spnFamilyMember = (Spinner) findViewById(R.id.spnFamilyMembers);
		spnQualification = (Spinner) findViewById(R.id.spnQualification);
		spnChildren = (Spinner) findViewById(R.id.spnChildren);

		spnAgeOne = (Spinner) findViewById(R.id.spnOne);
		spnAgeTwo = (Spinner) findViewById(R.id.spnTwo);
		spnAgeThree = (Spinner) findViewById(R.id.spnThree);
		spnGuardianQuali = (Spinner) findViewById(R.id.spnGurdnQuali);
		spnGuardianOCC = (Spinner) findViewById(R.id.spnGuardianOcc);
		spnHead = (Spinner) findViewById(R.id.spnHead);
		spnWaterSource = (Spinner) findViewById(R.id.spnWaterSource);
		spnSanitation = (Spinner) findViewById(R.id.spnSanitation);
		spnJoin = (Spinner) findViewById(R.id.spnJoin);
		spnJoiningPermission = (Spinner) findViewById(R.id.spnJoiningPermission);


		
		imageDOB.setOnClickListener(this);
		imageLMP.setOnClickListener(this);
		

		String [] guardianRelation = new String [] {"Husband", "Father", "Mother","Brother"};
		ArrayAdapter<String>  adapterGurdianrelation =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, guardianRelation);
		spnGuardianRelation.setAdapter(adapterGurdianrelation);


		String [] religion = new String [] {"Muslim", "Hindu", "Khristan", "Buddist"};
		ArrayAdapter<String>  adapterReligion =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, religion);
		spnReligion.setAdapter(adapterReligion);


		String [] bloodGroup = new String [] {"A+", "A-", "B+","B-", "O+", "O-","AB+","AB-"};
		ArrayAdapter<String>  adapterBloodgroup =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, bloodGroup);
		spnBloodGroup.setAdapter(adapterBloodgroup);


		String [] tribe = new String [] {"Sunni", "Khasia", "Sia","Mong"};
		ArrayAdapter<String>  adapterTribe =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, tribe);
		spnTribe.setAdapter(adapterTribe);


		String [] occupation = new String [] {"Job", "Bussiness", "Housewife"};
		ArrayAdapter<String>  adapterOccupation =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, occupation);
		spnOccupation.setAdapter(adapterOccupation);


		String [] familyMember = new String [] {"2","3","4","5","6"};
		ArrayAdapter<String> adapterFamilyMember = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,familyMember);
		spnFamilyMember.setAdapter(adapterFamilyMember);


		String [] qualification = new String []{"P.E.C","J.S.C","S.S.C","H.S.C","Honours"," Masters "};
		ArrayAdapter<String> adapterQualification = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,qualification);
		spnQualification.setAdapter(adapterQualification);

		String [] children = new String[] {"1","2","3"};
		ArrayAdapter<String> adapterChildren = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,children);
		spnChildren.setAdapter(adapterChildren);


		String [] ageOne = new String [] {"Null","1 Year","2 Years","3 Years","4 Years","5 Years","6 Years","7 Years","8 Years","9 Years","10 Years","11 Years","12 Years","13 Years"};
		ArrayAdapter<String> adapterAgeOne = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,ageOne);
		spnAgeOne.setAdapter(adapterAgeOne);


		String [] ageTwo = new String [] {"Null","1 Year","2 Years","3 Years","4 Years","5 Years","6 Years","7 Years","8 Years","9 Years","10 Years","11 Years","12 Years","13 Years"};
		ArrayAdapter<String> adapterAgeTwo = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, ageTwo);
		spnAgeTwo.setAdapter(adapterAgeTwo);


		String [] ageThree = new String [] {"Null","1 Year","2 Years","3 Years","4 Years","5 Years","6 Years","7 Years","8 Years","9 Years","10 Years","11 Years","12 Years","13 Years"};
		ArrayAdapter<String> adapterAgeThree = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,ageThree);
		spnAgeThree.setAdapter(adapterAgeThree);


//		   spnGuardianRelation.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//			@Override
//			public void onItemSelected(AdapterView<?> parent, View view,
//					int position, long id) {
//				
//				
//				etGuardianName.setFocusableInTouchMode(true);  //if this is not already set
//	            etGuardianName.requestFocus();  //to move the cursor
//	            final InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//	            inputMethodManager.showSoftInput(etGuardianName, InputMethodManager.SHOW_IMPLICIT);  
//	            
//				
//				
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> parent) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
		
		   spnChildren.setOnItemSelectedListener(new  OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				
				if(spnChildren.getSelectedItem().toString().equals("1"))
				{
					
					spnAgeOne.setVisibility(View.VISIBLE);
					spnAgeTwo.setVisibility(View.INVISIBLE);
					spnAgeThree.setVisibility(View.INVISIBLE);
					}
				else if (spnChildren.getSelectedItem().toString().equals("2"))
						{
					spnAgeOne.setVisibility(View.VISIBLE);
					spnAgeTwo.setVisibility(View.VISIBLE);
					spnAgeThree.setVisibility(View.INVISIBLE);
				}
				else if (spnChildren.getSelectedItem().toString().equals("3"))
				{
			spnAgeOne.setVisibility(View.VISIBLE);
			spnAgeTwo.setVisibility(View.VISIBLE);
			spnAgeThree.setVisibility(View.VISIBLE);
		}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				
				
			}
		});

		
		
		String [] guardianQualification = new String []{"P.E.C","J.S.C","S.S.C","H.S.C","Honours"," Masters "};
		ArrayAdapter<String> adapterGuardianQualification = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,guardianQualification);
		spnGuardianQuali.setAdapter(adapterGuardianQualification);



		String [] guardianOccupation = new String [] {"Job", "Bussiness", "Housewife","Farmer","Student"};
		ArrayAdapter<String>  adapterGuardianOccupation =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, guardianOccupation);
		spnGuardianOCC.setAdapter(adapterGuardianOccupation);


		String [] head = new String [] {"Yes", "No"};
		ArrayAdapter<String>  adapterHead =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, head);
		spnHead.setAdapter(adapterHead);


		String [] waterSource = new String [] {"Tubewel", "Pond", "Rain"};
		ArrayAdapter<String>  adapterWaterSourch =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, waterSource);
		spnWaterSource.setAdapter(adapterWaterSourch);



		String [] sanitation = new String []{"Bath Room", "Latrin","Open Field" };
		ArrayAdapter<String> adapterSanitation = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, sanitation);
		spnSanitation.setAdapter(adapterSanitation);
		
		
		String [] joining = new String [] {"Yes", "No"};
		ArrayAdapter<String> adapterJoining = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, joining);
		spnJoin.setAdapter(adapterJoining);
		
		
		String [] joiningPermission = new String [] {"Yes", "No"};
		ArrayAdapter<String> adapterJoiningPermission = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, joiningPermission);
		spnJoiningPermission.setAdapter(adapterJoiningPermission);
		
		
		
//		spnGuardianRelation.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//			@Override
//			public void onItemSelected(AdapterView<?> parent,
//					View view, int position, long id) {
//				
//				//String guardian = spnGuardianRelation.getSelectedItem().toString();
//				
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> parent) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
//		
		
		addBtnPrev = (ImageView) findViewById(R.id.addBtnPrev);
		addBtnPrev.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intt = new Intent( AddFormActivity.this, GroupDetailTabsActivity.class);
				startActivity(intt);
				
				
			}
		});
		
		
		
		
		imageAdd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
                startActivityForResult(cameraIntent, CAMERA_REQUEST); 
              

			}
		});
		
		
		
		
		

		btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				
				Intent intt = new Intent( AddFormActivity.this, GroupDetailTabsActivity.class);
				startActivity(intt);
				
				
				String tribe = spnTribe.getSelectedItem().toString();
				String occupation = spnOccupation.getSelectedItem().toString();
				String qualification = spnQualification.getSelectedItem().toString();
				String guardianQualification = spnGuardianQuali.getSelectedItem().toString();
				String guardianOccpation = spnGuardianOCC.getSelectedItem().toString();
				String waterSource = spnWaterSource.getSelectedItem().toString();
				String sanitation = spnSanitation.getSelectedItem().toString();
			
				
				
			    members.setName_en(etNameEn.getText().toString());
				members.setGuardian_name(etGuardianName.getText().toString());
				members.setAddress(etAddress.getText().toString());
				
				members.setRelation(spnGuardianRelation.getSelectedItem().toString());
				
				members.setReligion(spnReligion.getSelectedItem().toString());
				members.setBlood_group(spnBloodGroup.getSelectedItem().toString());
				members.setHead(spnHead.getSelectedItem().toString());
				members.setJoin(spnJoin.getSelectedItem().toString());
				members.setJoin_permission(spnJoiningPermission.getSelectedItem().toString());
				
				
				
				members.setTribe(tribe);
				members.setOccupation(occupation);
				members.setFamily_members(Integer.parseInt(spnFamilyMember.getSelectedItem().toString()));
				members.setQualification(qualification);
				
				members.setChildren(Integer.parseInt(spnChildren.getSelectedItem().toString()) );
				
				members.setAgeOne(spnAgeOne.getSelectedItem().toString());
				members.setAgeTwo(spnAgeTwo.getSelectedItem().toString());
				members.setAgeThree(spnAgeThree.getSelectedItem().toString());
				
			
				members.setGuardian_qual(guardianQualification);
				members.setGuardian_occ(guardianOccpation);
				//members.setHead(Boolean.parseBoolean(spnHead.getSelectedItem().toString()));
				members.setWater_source(waterSource);
				members.setSanitation(sanitation);
			
				
				//String image = storagePath.toString();
				//members.setPhoto(image);
				//members.setPhoto(storagePath.getAbsolutePath().toString());
				
				
				
				members.setPhoto(myImage.getAbsolutePath().toString());
				
				members.setClub_name(etClubName.getText().toString());
				members.setFamily_income(Integer.parseInt(etFamilyIncome.getText().toString()));
				members.setBook_no(Integer.parseInt(etBookNo.getText().toString()));
				members.setDob(txtDOB.getText().toString());
				members.setLmp(txtLMP.getText().toString());
				members.setPreg_cond(txtPresentPragnancy.getText().toString());
				
				Toast.makeText(getApplicationContext(), "Data Added Successfully!!", Toast.LENGTH_SHORT).show();
				
				members.save();
				
				
				txtDOB.setText("");
				txtLMP.setText("");
				txtPresentPragnancy.setText("");
				etNameEn.setText("");
				etGuardianName.setText("");
				etAddress.setText("");
				etClubName.setText("");
				etFamilyIncome.setText("");
				etBookNo.setText("");
				
				
				
			}
		});
		
		


	}
	private Members members = new Members();
	
	
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {  
            Bitmap imageBit = (Bitmap) data.getExtras().get("data"); 
            storagePath = new File(Environment.getExternalStorageDirectory() +"/DCIM/HumacLab/"); 
            storagePath.mkdirs();
             myImage = new File(storagePath, System.currentTimeMillis() + ".jpg");
            
            try 
            {
            	FileOutputStream out = new FileOutputStream(myImage);
            	imageBit.compress(Bitmap.CompressFormat.JPEG, 80, out); 
            	Uri outputFileUri = Uri.fromFile(myImage);
            	
               
            	
            	imageAdd.setImageBitmap(imageBit);
            	out.close();
            	//Toast.makeText(Review.this, "Image saved: " + outputFileUri.toString(), Toast.LENGTH_LONG).show();
				
			} catch (Exception e) 
			{
				
			}
            
            
        }  
    }


	@Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DATE_START_DIALOG_ID:
            return new DatePickerDialog(this,mDatepicker, startYear, startMonth, startDay);
        case PRAGNANT_DATE_START_DIALOG_ID:
            return new DatePickerDialog(this,pragnantDatepicker, startYear, startMonth, startDay);
         }
        return null;
     }
   
	private DatePickerDialog.OnDateSetListener mDatepicker = new DatePickerDialog.OnDateSetListener() {
		
		@Override
		public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
			
			//String month_name = dateFormat.format(selectedMonth+1);
			
			
			startYear = selectedYear;
			startMonth = selectedMonth;
			startDay = selectedDay;
			
			txtDOB.setText(  selectedDay + " - " + (selectedMonth+1) + " - " + selectedYear);

			
			
		}
	};
	
   private DatePickerDialog.OnDateSetListener pragnantDatepicker = new DatePickerDialog.OnDateSetListener() {
		
		@Override
		public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
			
			//String month_nameLMP = dateFormat.format(startMonth);
			
			
			startYearLMP = selectedYear;
			startMonthLMP = selectedMonth;
			startDayLMP = selectedDay;
			
			 age.setDateOfBirth(startYear, startMonth, startDay);
			//txtDOB.setText(  selectedDay + "," + (selectedMonth+1) + "," + selectedYear);
			txtLMP.setText( selectedDay + " - " + (selectedMonth +1));
			calculateResult();
			
			
		}
	};
	
	
	


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgDOB:
			showDialog(DATE_START_DIALOG_ID);
			
			break;
			
		case R.id.imgLMP:
			showDialog(PRAGNANT_DATE_START_DIALOG_ID);
			
			break;

		default:
			break;
		}
		
	}
	
	
	
public void calculateResult(){
		
		
		age.calculateMonth();
		age.calculateDay();
		
   
	txtPresentPragnancy.setText(age.getResult());
	
	}
	

	

	
}




