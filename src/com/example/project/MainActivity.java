	package com.example.project;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class MainActivity extends Activity {
	
	private int SPLASH_DISPLAY_LENGTH = 1000;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//android:theme="@android:style/Theme.Holo.NoActionBar"
		// android:theme="@style/AppTheme" 
		
		 new Handler().postDelayed(new Runnable(){
				@Override
				public void run() {
					/* Create an Intent that will start the Menu-Activity. */
					Intent mainIntent = new Intent(MainActivity.this,SignInActivity.class);
					MainActivity.this.startActivity(mainIntent);
					MainActivity.this.finish();
				}
			}, SPLASH_DISPLAY_LENGTH);
		
		
	}

	

	
}
