package com.example.project;

import static com.roscopeco.ormdroid.Query.eql;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.humacLab.animation.CalculateAge;
import com.humacLab.project.sqlite.Members;
import com.roscopeco.ormdroid.Entity;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class EditActivity extends Activity implements OnClickListener {

//	private static final int CAMERA_REQUEST = 1888;
//	public File myImage;
//	public File storagePath;
	
	
	
	 private int startYear = 2000;
	 private int startMonth = 6;
	 private int startDay = 1;
	 static final int DATE_START_DIALOG_ID = 0;
	 static final int PRAGNANT_DATE_START_DIALOG_ID = 2000;
	 CalculateAge age = new CalculateAge();
	 
	 Calendar calendar= Calendar.getInstance();
	 SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM");
	
	
	
	
	private ImageView btnEdtPrevious,imageEdit, imageEditDOB,imageEditLMP, imageEditTransfer;
	private Members members;
	private EditText edtname, edtGurdName, edtAddress, edtFamilyIncome, edtClubName, edtBookNo;
	private Button btnEditSave;
	private Spinner spnEdtGurdRelation, spnEdtTribe, spnEdtOcc, spnEdtFamilyMem, spnEdtEduQuali, spnEdtBloodGrp, spnEdtChildren, spnEdtFirstChild, spnEdtSecondChild, spnEdtThirdChild;
	private Spinner spnEdtReligion, spnEdtGurdQuali, spnEdtGurdOcc, spnEdtHead, spnEdtWaterSource, spnEdtSanitation, spnEdtJoin, spnEdtJoiningPermissin;
	private TextView txtEditDOB, txtEditLMP, txtEditPresentPragnancy;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editlay);
		
		
		
		
		
		imageEditTransfer = (ImageView) findViewById(R.id.imageEditTransfar);
		imageEditDOB = (ImageView) findViewById(R.id.imgEditDOB);
		imageEditLMP = (ImageView) findViewById(R.id.imgEditLMP);
		
		txtEditDOB = (TextView) findViewById(R.id.txtEditDOB);
		txtEditLMP = (TextView) findViewById(R.id.txtEditLMP);
		txtEditPresentPragnancy = (TextView) findViewById(R.id.txtEditPresentPragnancy);
		
		btnEditSave = (Button) findViewById(R.id.btnEditSave);
		imageEdit = (ImageView) findViewById(R.id.imageEdit);
		edtname = (EditText) findViewById(R.id.editEtNameEn);
		edtGurdName = (EditText) findViewById(R.id.editEtGuardianName);
		edtAddress = (EditText) findViewById(R.id.etEditAddress);
		edtClubName = (EditText) findViewById(R.id.etEditClubName);
		
		
		edtFamilyIncome = (EditText) findViewById(R.id.etEditFamilyIncome);
		edtBookNo = (EditText) findViewById(R.id.etEditBookNo);
		btnEdtPrevious = (ImageView) findViewById(R.id.editBtnPrev);
		
		spnEdtGurdRelation = (Spinner) findViewById(R.id.spnEditGuardianRelation);
		spnEdtReligion = (Spinner) findViewById(R.id.spnEditReligion);
		spnEdtTribe = (Spinner) findViewById(R.id.spnEditTribe);
		spnEdtOcc = (Spinner) findViewById(R.id.spnEditOccupation);
		spnEdtFamilyMem = (Spinner) findViewById(R.id.spnEditFamilyMembers);
		spnEdtEduQuali = (Spinner) findViewById(R.id.spnEditQualification);
		spnEdtBloodGrp  = (Spinner) findViewById(R.id.spnEditBloodGroup);
		spnEdtChildren = (Spinner) findViewById(R.id.spnEditChildren);
		spnEdtFirstChild = (Spinner) findViewById(R.id.spnEditOne);
		spnEdtSecondChild = (Spinner) findViewById(R.id.spnEditTwo);
		spnEdtThirdChild = (Spinner) findViewById(R.id.spnEditThree);
		spnEdtGurdQuali = (Spinner) findViewById(R.id.spnEditGurdnQuali);
		spnEdtGurdOcc = (Spinner) findViewById(R.id.spnEditGuardianOcc);
		spnEdtHead = (Spinner) findViewById(R.id.spnEditHead);
		spnEdtWaterSource = (Spinner) findViewById(R.id.spnEditWaterSource);
		spnEdtSanitation = (Spinner) findViewById(R.id.spnEditSanitation);
		spnEdtJoin = (Spinner) findViewById(R.id.spnEditJoin);
		spnEdtJoiningPermissin = (Spinner) findViewById(R.id.spnEditJoiningPermission);
		
		 
		int id = 0;
		 Bundle bun = getIntent().getExtras();
		 try{
			 id = bun.getInt("Members Id");
		 }catch(Exception e){}
		 
		  members = Entity.query(Members.class).where(eql("_id", id)).execute();
		 
		    
		  
		  imageEditDOB.setOnClickListener(this);
		  imageEditLMP.setOnClickListener(this);
		  
		  
		    String [] children = new String[] {"1","2","3"};
			ArrayAdapter<String> adapterEdtChildren = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,children);
			spnEdtChildren.setAdapter(adapterEdtChildren);


			String [] ageOne = new String [] {"Null","1 Year","2 Years","3 Years","4 Years","5 Years","6 Years","7 Years","8 Years","9 Years","10 Years","11 Years","12 Years","13 Years"};
			ArrayAdapter<String> adapterEdtAgeOne = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,ageOne);
			spnEdtFirstChild.setAdapter(adapterEdtAgeOne);


			String [] ageTwo = new String [] {"Null","1 Year","2 Years","3 Years","4 Years","5 Years","6 Years","7 Years","8 Years","9 Years","10 Years","11 Years","12 Years","13 Years"};
			ArrayAdapter<String> adapterEdtAgeTwo = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, ageTwo);
			spnEdtSecondChild.setAdapter(adapterEdtAgeTwo);


			String [] ageThree = new String [] {"Null","1 Year","2 Years","3 Years","4 Years","5 Years","6 Years","7 Years","8 Years","9 Years","10 Years","11 Years","12 Years","13 Years"};
			ArrayAdapter<String> adapterEdtAgeThree = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,ageThree);
			spnEdtThirdChild.setAdapter(adapterEdtAgeThree);

			
			
			
			spnEdtChildren.setOnItemSelectedListener(new  OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					
					if(spnEdtChildren.getSelectedItem().toString().equals("1"))
					{
						
						spnEdtFirstChild.setVisibility(View.VISIBLE);
						spnEdtSecondChild.setVisibility(View.INVISIBLE);
						spnEdtThirdChild.setVisibility(View.INVISIBLE);
						
						}
					else if (spnEdtChildren.getSelectedItem().toString().equals("2"))
							{
						
						spnEdtFirstChild.setVisibility(View.VISIBLE);
						spnEdtSecondChild.setVisibility(View.VISIBLE);
						spnEdtThirdChild.setVisibility(View.INVISIBLE);
						
						
						
					}
					else if (spnEdtChildren.getSelectedItem().toString().equals("3"))
					{
						
						
						spnEdtFirstChild.setVisibility(View.VISIBLE);
						spnEdtSecondChild.setVisibility(View.VISIBLE);
						spnEdtThirdChild.setVisibility(View.VISIBLE);
						
						
				
			}
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					
					
				}
			});

			
			
		  
		    String [] tribe = new String [] {"Sunni", "Khasia", "Sia","Mong","Bramon"};
			ArrayAdapter<String>  adapterEdtTribe =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, tribe);
			spnEdtTribe.setAdapter(adapterEdtTribe);
		  
			
			String [] occupation = new String [] {"Job", "Bussiness", "Housewife","Student"};
			ArrayAdapter<String>  adapterEdtOccupation =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, occupation);
			spnEdtOcc.setAdapter(adapterEdtOccupation);
			
			
			String [] familyMember = new String [] {"2","3","4","5","6"};
			ArrayAdapter<String> adapterEdtFamilyMember = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,familyMember);
			spnEdtFamilyMem.setAdapter(adapterEdtFamilyMember);
			
			
			String [] qualification = new String []{"P.E.C","J.S.C","S.S.C","H.S.C","Honours"," Masters "};
			ArrayAdapter<String> adapterEdtQualification = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,qualification);
			spnEdtEduQuali.setAdapter(adapterEdtQualification);
			
		  
			
			String [] bloodGroup = new String [] {"A+", "A-", "B+","B-", "O+", "O-","AB+","AB-"};
			ArrayAdapter<String>  adapterEdtBloodgroup =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, bloodGroup);
			spnEdtBloodGrp.setAdapter(adapterEdtBloodgroup);
			
			
			
			
		    String [] guardianRelation = new String [] {"Husband", "Father", "Mother", "Brother"};
			ArrayAdapter<String>  adapterEdtGurdianrelation =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, guardianRelation);
			spnEdtGurdRelation.setAdapter(adapterEdtGurdianrelation);
		  
		    
			String [] editReligion = new String [] {"Muslim", "Hindu", "Khristan", "Buddist"};
			ArrayAdapter<String>  adapterEdtReligion =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, editReligion);
			spnEdtReligion.setAdapter(adapterEdtReligion);
			
			
			String [] guardianQualification = new String []{"P.E.C","J.S.C","S.S.C","H.S.C","Honours"," Masters "};
			ArrayAdapter<String> adapterEdtGuardianQualification = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,guardianQualification);
			spnEdtGurdQuali.setAdapter(adapterEdtGuardianQualification);



			String [] guardianOccupation = new String [] {"Job", "Bussiness", "Housewife","Farmer"};
			ArrayAdapter<String>  adapterEdtGuardianOccupation =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, guardianOccupation);
			spnEdtGurdOcc.setAdapter(adapterEdtGuardianOccupation);


			String [] head = new String [] {"Yes", "No"};
			ArrayAdapter<String>  adapterEdtHead =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, head);
			spnEdtHead.setAdapter(adapterEdtHead);


			String [] waterSource = new String [] {"tubewel", "Pond", "Rain"};
			ArrayAdapter<String>  adapterEdtWaterSourch =  new  ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, waterSource);
			spnEdtWaterSource.setAdapter(adapterEdtWaterSourch);



			String [] sanitation = new String []{"Latrin","Open Field", "Bath Room"};
			ArrayAdapter<String> adapterEdtSanitation = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, sanitation);
			spnEdtSanitation.setAdapter(adapterEdtSanitation);
			
			
			String [] joining = new String [] {"Yes", "No"};
			ArrayAdapter<String> adapterEdtJoining = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, joining);
			spnEdtJoin.setAdapter(adapterEdtJoining);
			
			
			String [] joiningPermission = new String [] {"Yes", "No"};
			ArrayAdapter<String> adapterEdtJoiningPermission = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, joiningPermission);
			spnEdtJoiningPermissin.setAdapter(adapterEdtJoiningPermission);
			
			
			if(members.guardian_qual != null)
			{
				spnEdtGurdQuali.setSelection(adapterEdtGuardianQualification.getPosition(members.guardian_qual));
			}
			
			
			if(members.guardian_occ != null)
			{
				spnEdtGurdOcc.setSelection(adapterEdtGuardianOccupation.getPosition(members.guardian_occ));
			}
			
			

			if(members.head != null)
			{
				spnEdtHead.setSelection(adapterEdtHead.getPosition(members.head));
			}
			
			

			if(members.water_source != null)
			{
				spnEdtWaterSource.setSelection(adapterEdtWaterSourch.getPosition(members.water_source));
			}
			
			

			if(members.sanitation != null)
			{
				spnEdtSanitation.setSelection(adapterEdtSanitation.getPosition(members.sanitation));
			}
			
			
			if(members.join != null)
			{
				spnEdtJoin.setSelection(adapterEdtJoining.getPosition(members.join));
			}
			
			

			if(members.join_permission != null)
			{
				spnEdtJoiningPermissin.setSelection(adapterEdtJoiningPermission.getPosition(members.join_permission));
			}
			
			
			
			if(Integer.toString(members.children) != null)
			{
				spnEdtChildren.setSelection(adapterEdtChildren.getPosition(Integer.toString(members.children)));
			}
			
			
			if(members.ageOne != null)
			{
				spnEdtFirstChild.setSelection(adapterEdtAgeOne.getPosition(members.ageOne));
			}
			
			
			
			if(members.ageTwo != null)
			{
				spnEdtSecondChild.setSelection(adapterEdtAgeTwo.getPosition(members.ageTwo));
			}
			
			
			if(members.ageThree != null)
			{
				spnEdtThirdChild.setSelection(adapterEdtAgeThree.getPosition(members.ageThree));
			}
			
			
			
			
			
			if(members.tribe != null)
			{
				spnEdtTribe.setSelection(adapterEdtTribe.getPosition(members.tribe));
			}
			
			
			if(members.occupation != null)
			{
				spnEdtOcc.setSelection(adapterEdtOccupation.getPosition(members.occupation));
			}
			
			
			if(members.religion != null)
			{
				spnEdtReligion.setSelection(adapterEdtReligion.getPosition(members.religion));
			}
			
		   
			if(members.relation != null)
			{
				spnEdtGurdRelation.setSelection(adapterEdtGurdianrelation.getPosition(members.relation));
			}
			
		  
		  
			if(Integer.toString(members.family_members) != null)
			{
				spnEdtFamilyMem.setSelection(adapterEdtFamilyMember.getPosition(Integer.toString(members.family_members)));
			}
			
		  
			if(members.qualification != null)
			{
				spnEdtEduQuali.setSelection(adapterEdtQualification.getPosition(members.qualification));
			}
			
		   
			if(members.blood_group != null)
			{
				spnEdtBloodGrp.setSelection(adapterEdtBloodgroup.getPosition(members.blood_group));
			}
			
			
		  
			
			 String filePath = members.getPhoto();
			 File file = new File(filePath);
			 Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
			 imageEdit.setImageBitmap(bitmap);
			 imageEditTransfer.setImageBitmap(bitmap);
			
			
			btnEdtPrevious.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intt = new Intent(EditActivity.this, GroupDetailTabsActivity.class);
					startActivity(intt);
					
				}
			});
			
			
		  
		txtEditDOB.setText(members.dob);
		txtEditLMP.setText(members.lmp);
		txtEditPresentPragnancy.setText(members.preg_cond);
		
		edtname.setText(members.name_en);
		edtGurdName.setText(members.guardian_name);
		edtAddress.setText(members.address);
		edtClubName.setText(members.club_name);
		
		edtBookNo.setText(Integer.toString(members.book_no));
		edtFamilyIncome.setText(Integer.toString(members.family_income));
		
		
		
		
//    imageEdit.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				
//				
//				Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
//                startActivityForResult(cameraIntent, CAMERA_REQUEST); 
//              
//
//			}
//		});
//		
		
		
		
  
	    
		
		btnEditSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				

				Intent intt = new Intent( EditActivity.this, GroupDetailTabsActivity.class);
				startActivity(intt);
				
				members.setReligion(spnEdtReligion.getSelectedItem().toString());
				
				members.setRelation(spnEdtGurdRelation.getSelectedItem().toString());
				members.setTribe(spnEdtTribe.getSelectedItem().toString());
				members.setOccupation(spnEdtOcc.getSelectedItem().toString());
				members.setFamily_members(Integer.parseInt(spnEdtFamilyMem.getSelectedItem().toString()));
				members.setQualification(spnEdtEduQuali.getSelectedItem().toString());
				members.setBlood_group(spnEdtBloodGrp.getSelectedItem().toString());
				members.setChildren(Integer.parseInt(spnEdtChildren.getSelectedItem().toString()));
				members.setAgeOne(spnEdtFirstChild.getSelectedItem().toString());
				members.setAgeTwo(spnEdtSecondChild.getSelectedItem().toString());
				members.setAgeThree(spnEdtThirdChild.getSelectedItem().toString());
				
				members.setGuardian_qual(spnEdtGurdQuali.getSelectedItem().toString());
				members.setGuardian_occ(spnEdtGurdOcc.getSelectedItem().toString());
				members.setHead(spnEdtHead.getSelectedItem().toString());
				members.setWater_source(spnEdtWaterSource.getSelectedItem().toString());
				members.setSanitation(spnEdtSanitation.getSelectedItem().toString());
				members.setJoin(spnEdtJoin.getSelectedItem().toString());
				members.setJoin_permission(spnEdtJoiningPermissin.getSelectedItem().toString());
				
				
				members.dob = txtEditDOB.getText().toString();
				members.lmp= txtEditLMP.getText().toString();
				members.preg_cond = txtEditPresentPragnancy.getText().toString();
				
				members.name_en = edtname.getText().toString();
				members.guardian_name = edtGurdName.getText().toString();
				members.address = edtAddress.getText().toString();
				members.club_name = edtClubName.getText().toString();
				
			    members.family_income = Integer.parseInt(edtFamilyIncome.getText().toString());
				members.book_no =  Integer.parseInt(edtBookNo.getText().toString());

				Toast.makeText(getApplicationContext(), "Data Updated Successfully!!", Toast.LENGTH_SHORT).show();
				members.save();
				
				
				
				
				
//				txtEditDOB.setText("");
//				txtEditLMP.setText("");
//				txtEditPresentPragnancy.setText("");
//				
//				edtname.setText("");
//				edtGurdName.setText("");
//				edtAddress.setText("");
//				edtClubName.setText("");
//				edtFamilyIncome.setText("");
//				edtBookNo.setText("");
				
			}
		});
		
		
		

		
		
	}

	
	
	@Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DATE_START_DIALOG_ID:
            return new DatePickerDialog(this,mDatepicker, startYear, startMonth, startDay);
        case PRAGNANT_DATE_START_DIALOG_ID:
            return new DatePickerDialog(this,pragnantDatepicker, startYear, startMonth, startDay);
         }
        return null;
     }
   
	private DatePickerDialog.OnDateSetListener mDatepicker = new DatePickerDialog.OnDateSetListener() {
		
		@Override
		public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
			
	     //String month_name = dateFormat.format(selectedMonth+1);
			
			startYear = selectedYear;
			startMonth = selectedMonth;
			startDay = selectedDay;
			
			txtEditDOB.setText(  selectedDay + " - " + (selectedMonth+1) + " - " + selectedYear);

			
			
		}
	};
	
   private DatePickerDialog.OnDateSetListener pragnantDatepicker = new DatePickerDialog.OnDateSetListener() {
		
		@Override
		public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
			
			String month_nameLMP = dateFormat.format(selectedMonth+1);
			
			startYear = selectedYear;
			startMonth = selectedMonth;
			startDay = selectedDay;
			age.setDateOfBirth(startYear, startMonth, startDay);
			//txtDOB.setText(  selectedDay + "," + (selectedMonth+1) + "," + selectedYear);
			txtEditLMP.setText(  selectedDay + " " + (month_nameLMP));
			calculateResult();
			
			
		}
	};
	
	
	


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgEditDOB:
			showDialog(DATE_START_DIALOG_ID);
			
			break;
			
		case R.id.imgEditLMP:
			showDialog(PRAGNANT_DATE_START_DIALOG_ID);
			
			break;

		default:
			break;
		}
		
	}
	
	
	
public void calculateResult(){
		
		
		age.calculateMonth();
		age.calculateDay();
		
	
	txtEditPresentPragnancy.setText(age.getResult());
	
	}
	

	
	

	
}
