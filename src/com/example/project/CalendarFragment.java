package com.example.project;


import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class CalendarFragment extends Fragment implements OnDateSetListener {
	View view;
	Activity activity;
	
	
	
    public static final String DATEPICKER_TAG = "datepicker";
    
    

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
		
		view = inflater.inflate(R.layout.calendar, container, false);
		activity = getActivity();

        
		
		AlertDialog alertDialog = new AlertDialog.Builder(view.getContext()).create();
		alertDialog.setTitle(" Chapter Name");
		alertDialog.setMessage("Choosing childcare means making sure that your child is safe and happy in an environment that's fun, educational, and loving");
		alertDialog.show();
		
		
		
        
     
        final Calendar calendar = Calendar.getInstance();

        final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
       
        datePickerDialog.setYearRange(1985, 2028);
       
        
        FragmentManager fm = getFragmentManager(); 
        FragmentTransaction ft = fm.beginTransaction(); 
        ft.replace(R.id.plceholder, datePickerDialog); 
        ft.commit();
        

        if (savedInstanceState != null) {
            DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag(DATEPICKER_TAG);
            if (dpd != null) {
                dpd.setOnDateSetListener(this);
            }


        }
		return view;
    }



    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        Toast.makeText(getActivity(), "new date:" + year + "-" + month + "-" + day, Toast.LENGTH_LONG).show();
    }

   
    
    
}
