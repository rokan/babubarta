package com.example.project;

import com.roscopeco.ormdroid.ORMDroidApplication;

import android.app.Application;

public class MyApplication extends Application{

	public MyApplication(){

	}
	@Override
	public void onCreate() {
		super.onCreate();
		ORMDroidApplication.initialize(this);	
	}
}