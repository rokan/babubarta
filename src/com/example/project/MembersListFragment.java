package com.example.project;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.projectAdapters.MembersListAdapter;
import com.humacLab.project.sqlite.Members;
import com.roscopeco.ormdroid.Entity;

public class MembersListFragment extends Fragment {

	View viewm;
	Activity activity;
	MembersListAdapter adapter;
	ListView lstMembers;
	ImageView imageView;
	
	public static String [] prgmNameList={"Group 1","Group 2","Group 3","Group 4","Group 5","Group 6","Group 7","Group 8","Group 9"};

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
		
		
		
		viewm = inflater.inflate(R.layout.activity_members_list_fragment, container, false);
		activity = getActivity();
		
		imageView = (ImageView) viewm.findViewById(R.id.addImage);
		lstMembers = (ListView) viewm.findViewById(R.id.lstMembers);
		List<Members>member_list = Entity.query(Members.class).executeMulti();
//		adapter = new MembersListAdapter(activity, prgmNameList);
		
		adapter = new MembersListAdapter(getActivity(), R.layout.listitem_members, member_list);
		lstMembers.setAdapter(adapter);
		
		
//		SQLiteDatabase db = this.getReadableDatabase();
//	    Cursor c = db.rawQuery(selectQuery, null);
//	    
		
		
		
		
		
		lstMembers.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				if (firstVisibleItem + visibleItemCount >= totalItemCount) {
					// End has been reached
					// btnaddOutlet.startAnimation(AnimationUtils.loadAnimation(Outlet_List.this,
					// android.R.anim.fade_out));
					imageView.setVisibility(View.GONE);
				} else {
					// btnaddOutlet.startAnimation(AnimationUtils.loadAnimation(Outlet_List.this,
					// android.R.anim.fade_in));
					imageView.setVisibility(View.VISIBLE);
				}
				if (visibleItemCount == totalItemCount)
					imageView.setVisibility(View.VISIBLE);
			}
		});
	
		
		
		
		imageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				

				Intent intent = new Intent(getActivity(), AddFormActivity.class);
			    startActivity(intent);
				
				
			}
		});
		
		
		
		
		return viewm;
		
		
		
		
	}

	

	
}
