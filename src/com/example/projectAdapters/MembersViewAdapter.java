package com.example.projectAdapters;

import java.util.List;
import com.example.project.R;
import com.humacLab.project.sqlite.Members;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MembersViewAdapter extends ArrayAdapter<Members> {

	private Context context;
	List<Members> viewMembers;
	
	
	public MembersViewAdapter(Context context, int resource,List<Members> viewMembers) {
		super(context, R.layout.viewlay,  viewMembers);
	    this.context = context;
	    inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;
	    this.viewMembers = viewMembers;
		
	}
	private static LayoutInflater inflater=null;


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return viewMembers.size();
	}
	@Override
	public Members getItem(int position) {
		// TODO Auto-generated method stub
		return viewMembers.get(position);
	}
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	
	
	public class Holder{
		
		TextView viewName, viewGuardienAddress;
		
		
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		
		final Holder holder = new  Holder();
		View memView;
		memView = inflater.inflate(R.layout.viewlay, parent, false);
		holder.viewName = (TextView) memView.findViewById(R.id.viewName);
		holder.viewGuardienAddress = (TextView) memView.findViewById(R.id.viewGuardienName);
		memView.setTag(holder);
		
		Members viewMemmm = (Members)getItem(position);
		
		holder.viewName.setText(viewMemmm.getName_en()+"");
		holder.viewGuardienAddress.setText(viewMemmm.getGuardian_name());
		
		
		
//		holder.txtLstName.setText(mem.getName_en() + "");
//		holder.txtLstAddress.setText(mem.getAddress() + "");
//		
		
		
		
		return memView;
	}

}
