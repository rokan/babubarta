package com.example.projectAdapters;





import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.project.GroupDetailTabsActivity;
import com.example.project.R;

public class CustomeAdapter extends BaseAdapter {
	
	String [] result;
    Context context;
    
    private static LayoutInflater inflater = null;
	
	public CustomeAdapter( Context context, String [] groupLeaderList) {
	
		result = groupLeaderList;
		this.context = context;
		
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		
		return  result.length;
	}

	@Override
	public Object getItem(int position) {
		
		return position;
	}

	@Override
	public long getItemId(int position) {
		
		return position;
	}
	
	public class Holder{
		
		TextView tv;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		 Holder holder = new Holder();
		 View rowView;
		 rowView = inflater.inflate(R.layout.griditem_groups, null);
			
		 holder.tv=(TextView) rowView.findViewById(R.id.txtGroupName);

			holder.tv.setText(result[position]);

			rowView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//Toast.makeText(context, "You Clicked "+result[position], Toast.LENGTH_LONG).show();
					Intent intent = new Intent(context,GroupDetailTabsActivity.class);
					intent.putExtra("groupnumber", position+1);
					context.startActivity(intent);
				}
			});

			return rowView;
		}

	}
