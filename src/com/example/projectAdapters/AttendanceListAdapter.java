package com.example.projectAdapters;

import java.io.File;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.AttendanceListFragment;
import com.example.project.R;
import com.humacLab.project.sqlite.Members;
import com.roscopeco.ormdroid.Entity;

public class AttendanceListAdapter extends ArrayAdapter<Members> {

	List<Members> lst;
	String varTxtResult;
	String[] result;
	Context context;
	int id;

	private static LayoutInflater inflater = null;

	public AttendanceListAdapter(Context context, int resource,
			List<Members> objects, int id) {
		super(context, resource, objects);

		// result=prgmNamedList;

		this.lst = objects;
		this.context = context;
		this.id = id;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lst.size();
	}

	@Override
	public Members getItem(int position) {
		// TODO Auto-generated method stub
		return lst.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public class Holder {

		ImageView imagePhoto;
		ImageView imgCheck;
		ImageView imgUncheck;
		TextView txtAttendance;

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final Holder holder = new Holder();
		View rowView;

		rowView = inflater.inflate(R.layout.listitem_attandence, parent, false);

		holder.txtAttendance = (TextView) rowView
				.findViewById(R.id.attendanceName);

		holder.imagePhoto = (ImageView) rowView.findViewById(R.id.imgPhoto);
		holder.imgCheck = (ImageView) rowView.findViewById(R.id.imgCheck);
		holder.imgUncheck = (ImageView) rowView.findViewById(R.id.imgUnCheck);

		// Intent intent = new Intent();
		// int attendanceId = lst.get(position).id;
		// intent.putExtra("Members_Attendace_QR", attendanceId);
		// context.startActivity(intent);
		//

		// Intent it = ((Activity) context).getIntent();
		// if (it != null) {
		// String params = it.getStringExtra("res");
		// if (params != null) {
		//
		// varTxtResult = params;
		//
		// List<Members> members = Entity.query(Members.class)
		// .executeMulti();
		//
		// for (int i = 0; i < members.size(); i++) {
		// int pos = i + 1;
		// if (params.equals("" + members.get(i).getId())) {
		//
		// //imgUnCheck.setVisibility(View.GONE);
		// //imgCheck.setSelected(true);
		//
		// holder.imgCheck.setSelected(true);
		//
		// Toast.makeText(context,
		// "Id Matched Successfully!!",Toast.LENGTH_SHORT).show();
		// }
		//
		//
		// }
		//
		// }
		// }

		int clickedpositon = id - 1;
		if (position == clickedpositon) {

			if (rowView.isEnabled()) {
				if (holder.imgCheck.getVisibility() == View.GONE) {
					holder.imgCheck.setVisibility(View.VISIBLE);
					rowView.setEnabled(false);
					rowView.setOnClickListener(null);
				} else {
					holder.imgCheck.setVisibility(View.GONE);
				}

			}

		}

		rowView.setTag(holder);

		Members mem2 = (Members) getItem(position);

		String path = mem2.getPhoto();
		File file = new File(path);
		Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
		holder.imagePhoto.setImageBitmap(bitmap);

		holder.txtAttendance.setText(mem2.getName_en() + "");

		rowView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Toast.makeText(context, "You Clicked "+result[position],
				// Toast.LENGTH_LONG).show();
				if (holder.imgCheck.getVisibility() == View.GONE) {
					holder.imgCheck.setVisibility(View.VISIBLE);
				} else {
					holder.imgCheck.setVisibility(View.GONE);
				}
			}
		});
		return rowView;
	}
}
