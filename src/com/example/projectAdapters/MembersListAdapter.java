package com.example.projectAdapters;

import java.io.File;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.project.EditActivity;
import com.example.project.R;
import com.example.project.ViewActivity;
import com.humacLab.project.sqlite.Members;

public class MembersListAdapter extends ArrayAdapter<Members> implements AnimationListener{
   
	
	
	private View mLastView;
	private int mLastPosition;
	private int mLastVisibility;
	
	
	
	public Animation animMove;
	List<Members>list;
	public MembersListAdapter(Context context, int resource,List<Members> objects) {
		super(context, resource, objects);
		
		mLastPosition = -1;
		
		this.context=context;
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.list = objects;
	}
	//	String [] result;
	Context context;

	private static LayoutInflater inflater=null;

	//	public MembersListAdapter(Context context, String[] prgmNameList) {
	//		// TODO Auto-generated constructor stub
	//		result=prgmNameList;
	//		this.context=context;
	//		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	//
	//	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Members getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public class Holder
	{
		ImageView imgMenu,imageList;
		ImageView imgMenuLayer;
		LinearLayout lnEditLayer, editButtonLayer, viewButtonLayer;
		Animation animMove;
		ListView listMem;
 
		View hint;
		TextView txtEdit;
		TextView txtView, txtLstName, txtLstAddress;

	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final Holder holder=new Holder();
		View rowView;
		
		rowView = inflater.inflate(R.layout.listitem_members, parent,false);

     
		
        holder.listMem = (ListView) rowView.findViewById(R.id.lstMembers);
        
		holder.imgMenu=(ImageView) rowView.findViewById(R.id.imgMenu);
		
		holder.txtEdit = (TextView) rowView.findViewById(R.id.txtEdit);
		holder.txtView = (TextView) rowView.findViewById(R.id.txtView);
		
		
		holder.imageList = (ImageView) rowView.findViewById(R.id.imageList);
		holder.txtLstName = (TextView) rowView.findViewById(R.id.idName);
		holder.txtLstAddress = (TextView) rowView.findViewById(R.id.idAddress);
		
		holder.editButtonLayer = (LinearLayout) rowView.findViewById(R.id.editButtonLayer);
		holder.viewButtonLayer = (LinearLayout) rowView.findViewById(R.id.viewButtonLayer);
		holder.lnEditLayer = (LinearLayout) rowView.findViewById(R.id.lnEditLayer);
		
		
		
		
		
		
		 
		 
		
		
		
		
		//holder.listMem.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		//holder.listMem.setItemChecked(2 , true);
				
		
		
//holder.hint = rowView.findViewById(R.id.lnEditLayer);
//		listView.setAdapter(adatper)
//		listView.setSelection(0);
//		currentSelectedView = listView.getChildAt(0);
//		highlightCurrentRow(currentSelectedView);
		

//		holder.imgMenu = (ImageView) rowView.findViewById(R.id.imgMenu);
//		holder.imgMenu.setSelected(true);
		
//		holder.animMove = AnimationUtils.loadAnimation(getContext(), R.anim.right_left);
//		holder.animMove.setAnimationListener(this);
//		holder.lnEditLayer.startAnimation(holder.animMove);
//		
		//holder.lnEditLayer.overridePendingTransition(R.anim.right_left , R.anim.left_right);
		
		//getListView().setItemChecked(selectedGroupIndex, true);
		
//		
//		listView.notifyDataSetChanged();
//		listView.requestFocusFromTouch();
//		listView.setSelection(position);
		
		
//		holder.lnEditLayer.notify();
//		holder.lnEditLayer.requestFocusFromTouch();
//		holder.lnEditLayer.setSelected(false);
//		
		
//		ListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE); 
//		ListView.CHOICE_MODE_MULTIPLE
//		
//		ListView.setSelection(position) or ListView.setItemChecked(position, true);
//		
		
		
	//	holder.lnEditLayer.setSelected(true);
		
		
		
         
		
		rowView.setTag(holder);
		
		
//		 String filePath = members.getPhoto();
//		 File file = new File(filePath);
//		 Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
//		 btnViewImage.setImageBitmap(bitmap);

		
		
		Members mem = (Members) getItem(position);
		
		 String path = mem.getPhoto();
		 File file = new File(path);
		 Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
		 holder.imageList.setImageBitmap(bitmap);

		 
		holder.txtLstName.setText(mem.getName_en() + "");
		holder.txtLstAddress.setText(mem.getAddress() + "");
		
		//holder.imageList.set
		
		
		
		
//		if(holder.getShareButtons().getVisibility()==View.GONE){
//            holder.getShareButtons().setVisibility(View.VISIBLE);
//
//        }
//        else{
//            holder.getShareButtons().setVisibility(View.GONE);
//
//        }
		
		
		
        holder.imgMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				View prevView;
				
				if(holder.lnEditLayer.getVisibility()==View.GONE){
					holder.lnEditLayer.setVisibility(View.VISIBLE);
				}
			}
		});

		holder.imgMenuLayer=(ImageView) rowView.findViewById(R.id.imgMenuLayer);
		holder.imgMenuLayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(holder.lnEditLayer.getVisibility()==View.GONE)
				{
					holder.lnEditLayer.setVisibility(View.VISIBLE);

				}

			}
		});


		holder.editButtonLayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Intent intent = new Intent(context, EditActivity.class);
				int id = list.get(position).id;
				intent.putExtra("Members Id", id);
				context.startActivity(intent);

			}


		});

		holder.viewButtonLayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				
				Intent myIntent = new Intent(context, ViewActivity.class);
				int id = list.get(position).id;
			    myIntent.putExtra("Members Id", id);
				context.startActivity(myIntent);
				
		}


		});
		/*holder.tv=(TextView) rowView.findViewById(R.id.txtGroupName);

		holder.tv.setText(result[position]);

		rowView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Toast.makeText(context, "You Clicked "+result[position], Toast.LENGTH_LONG).show();
				Intent intent = new Intent(context,GroupDetailTabsActivity.class);
				intent.putExtra("group_number", position+1);
				context.startActivity(intent);
			}
		});*/

		return rowView;
	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		if (animation == animMove) { }
		
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
	
	
//	private void showFragment(final Fragment fragment)
//	{
//	    if (null == fragment)
//	        return;
//
//	    FragmentTransaction ft = getFragmentManager().beginTransaction();
//
//	    ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
//
//	    ft.replace(R.layout.activity_members_list_fragment, fragment, fragment.getClass().getSimpleName()).commit();
//
//	}

}
