package com.humacLab.animation;

import java.util.Calendar;

public class CalculateAge {
	
	private int startYear = 2010;
	private int startMonth = 12;
	private int startDay = 25;
	private int endYear;
	private int endMonth;
	private int endDay;
	private int resultYear;
	private int resultMonth;
	private int resultDay;
	private Calendar start;
	private Calendar end;
	
	
	public void setDateOfBirth(int sYear, int sMonth, int sDay){
		
		startYear = sYear;
		startMonth = sMonth;
//		startMonth++;
		startDay = sDay;
	}
	
	public long getSecond(){
		
		start = Calendar.getInstance();
		start.set(Calendar.YEAR, startYear);
		start.set(Calendar.MONTH, startMonth);
		start.set(Calendar.DAY_OF_MONTH, startDay);
		start.set(Calendar.HOUR, 12);
		start.set(Calendar.MINUTE, 30);
		start.set(Calendar.SECOND, 30);
		start.set(Calendar.MILLISECOND, 30);
		long now = end.getTimeInMillis();
		long old = start.getTimeInMillis();
		long diff = old - now;
		return diff/1000;
		
	}
	
	
	public String getResult(){
		
		return  resultMonth + " Months " + resultDay + " Days" ;
		
	}
	
	public String currentDate(){
		
		end = Calendar.getInstance();
		endYear = end.get(Calendar.YEAR);
		endMonth = end.get(Calendar.MONTH);
		endMonth++;
		endDay = end.get(Calendar.DAY_OF_MONTH);
		
		return endDay + ":" + endMonth + ":" + endYear;
		
	}
	
	
	public void calculateYear(){
		
		resultYear = endYear - startYear;
		
	}
	
	public void calculateMonth(){
		
		if(endMonth >= startMonth)
		{
			resultMonth = endMonth - startMonth;
		}
		else
		{
			resultMonth = endMonth - startMonth;
			resultMonth = 12 + resultMonth;
			resultYear--;
		}
	}
	
	
	public void calculateDay(){
		
		if(endDay >= startDay)
		{
			resultDay = endDay - startDay;
		}
		else
		{
			resultDay = endDay -startDay;
			resultDay = 30 + resultDay;
			if(resultMonth == 0)
			{
				resultMonth = 11;
				resultYear--;
			}
			else
			{
				resultMonth--;
			}
		}
	}
		
}
