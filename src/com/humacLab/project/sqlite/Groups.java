package com.humacLab.project.sqlite;

public class Groups {
	
	
	private int id;
	private String group_name;
	
	
	public Groups(){}
	
	public Groups(int id, String group_name) {
		super();
		this.id = id;
		this.group_name = group_name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGroup_name() {
		return group_name;
	}
	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

}
