package com.humacLab.project.sqlite;

//import java.util.Date;
import java.sql.Date;

public class Meetings {
	
	
	private int id;
	private Date date;
	private String chapter;
	private String topic;
	
	
	public Meetings (){};
	
	public Meetings(int id, Date date, String chapter, String topic) {
		super();
		this.id = id;
		this.date = date;
		this.chapter = chapter;
		this.topic = topic;
	}
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getChapter() {
		return chapter;
	}
	public void setChapter(String chapter) {
		this.chapter = chapter;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}

}
