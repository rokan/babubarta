package com.humacLab.project.sqlite;


import java.util.Date;

import com.roscopeco.ormdroid.Column;
import com.roscopeco.ormdroid.Entity;
import com.roscopeco.ormdroid.Table;

@Table(name="Member_Table")

public class Members extends Entity{
	
	@Column(name="_id")
	public int id;
	
	public int group_id;
	public String photo;
	public String name_bn;
	public String name_en;
	public String guardian_name;
	public String relation;
	public String dob;
	public String religion;
	public String blood_group;
	public String tribe;
	public String occupation;
	public int family_members;
	public String qualification;
	public String address;
	public String lmp;
	public String preg_cond;
	public int children;
	public int family_income;
	
	


	public String ageOne;
	public String ageTwo;
	public String ageThree;
	
	public String guardian_qual;
	public String guardian_occ;
   
	public String head;
	
	@Column(name="member_join")
	public String join;
	
	public String getJoin() {
		return join;
	}


	public void setJoin(String join) {
		this.join = join;
	}


	public String getJoin_permission() {
		return join_permission;
	}


	public void setJoin_permission(String join_permission) {
		this.join_permission = join_permission;
	}


	@Column(name="member_join_permission")
	public String join_permission;
	
	
	
	public String water_source;
	public String sanitation;
	
	
	
	
	
	public String club_name;
	public int book_no;
	
	
	
	public Members(){
		
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getGroup_id() {
		return group_id;
	}
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getName_bn() {
		return name_bn;
	}
	public void setName_bn(String name_bn) {
		this.name_bn = name_bn;
	}
	public String getName_en() {
		return name_en;
	}
	public void setName_en(String name_en) {
		this.name_en = name_en;
	}
	public String getGuardian_name() {
		return guardian_name;
	}
	public void setGuardian_name(String guardian_name) {
		this.guardian_name = guardian_name;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	
	public String getDob() {
		return dob;
	}


	public void setDob(String dob) {
		this.dob = dob;
	}


	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getBlood_group() {
		return blood_group;
	}
	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}
	public String getTribe() {
		return tribe;
	}
	public void setTribe(String tribe) {
		this.tribe = tribe;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public int getFamily_members() {
		return family_members;
	}
	public void setFamily_members(int family_members) {
		this.family_members = family_members;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public int getChildren() {
		return children;
	}
	public void setChildren(int children) {
		this.children = children;
	}
	public String getGuardian_qual() {
		return guardian_qual;
	}
	public void setGuardian_qual(String guardian_qual) {
		this.guardian_qual = guardian_qual;
	}
	public String getGuardian_occ() {
		return guardian_occ;
	}
	public String getLmp() {
		return lmp;
	}


	public void setLmp(String lmp) {
		this.lmp = lmp;
	}


	public String getPreg_cond() {
		return preg_cond;
	}


	public void setPreg_cond(String preg_cond) {
		this.preg_cond = preg_cond;
	}


	public void setGuardian_occ(String guardian_occ) {
		this.guardian_occ = guardian_occ;
	}
	
	
	
	
	public String getWater_source() {
		return water_source;
	}
	public void setWater_source(String water_source) {
		this.water_source = water_source;
	}
	public String getSanitation() {
		return sanitation;
	}
	public void setSanitation(String sanitation) {
		this.sanitation = sanitation;
	}
	
	
	
	
	
	public String getHead() {
		return head;
	}


	public void setHead(String head) {
		this.head = head;
	}


	

	
	

	


	public String getClub_name() {
		return club_name;
	}
	public void setClub_name(String club_name) {
		this.club_name = club_name;
	}
	public int getBook_no() {
		return book_no;
	}
	public void setBook_no(int book_no) {
		this.book_no = book_no;
	}
	
	
	



	

	
	
	public String getAgeOne() {
		return ageOne;
	}


	public void setAgeOne(String ageOne) {
		this.ageOne = ageOne;
	}


	public String getAgeTwo() {
		return ageTwo;
	}


	public void setAgeTwo(String ageTwo) {
		this.ageTwo = ageTwo;
	}


	public String getAgeThree() {
		return ageThree;
	}


	public void setAgeThree(String ageThree) {
		this.ageThree = ageThree;
	}


	public double getFamily_income() {
		return family_income;
	}


	public void setFamily_income(int family_income) {
		this.family_income = family_income;
	}
	
	

}
