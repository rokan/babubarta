package com.humacLab.project.sqlite;

import java.sql.Date;
//import java.util.Date;

public class Attendances {
	 
	private int id;
	private int member_id;
	private Date date;
	private String type;
	
		
	
	public Attendances(){};
	
	
	public Attendances(int id, int member_id, Date date, String type) {
		super();
		this.id = id;
		this.member_id = member_id;
		this.date = date;
		this.type = type;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMember_id() {
		return member_id;
	}
	public void setMember_id(int member_id) {
		this.member_id = member_id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	

}
