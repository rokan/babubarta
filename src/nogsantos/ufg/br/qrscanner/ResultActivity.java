package nogsantos.ufg.br.qrscanner;

import static com.roscopeco.ormdroid.Query.eql;

import java.util.List;

import com.example.project.AttendanceListFragment;
import com.example.project.GroupDetailTabsActivity;
import com.example.project.MainActivity;
import com.example.project.R;
import com.example.projectAdapters.AttendanceListAdapter;
import com.humacLab.project.sqlite.Members;
import com.roscopeco.ormdroid.Entity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import nogsantos.ufg.br.qrscanner.Utilities.Functions;
import nogsantos.ufg.br.qrscanner.Utilities.HardwareUtils;

public class ResultActivity extends Activity {

	Context context;
	AttendanceListAdapter attendanceListAdapter;
	EditText txtResult;
	Button btSave;
	String varTxtResult;
	ImageView imgCheck;
	ImageView imgUnCheck;

	// LayoutInflater inflater =
	// (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	/**
	 * Create
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_attendance_list);

		// imgCheck = (ImageView)findViewById(R.id.imgCheck);
		// imgUnCheck = (ImageView)findViewById(R.id.imgUnCheck);

		// btSave = (Button) findViewById(R.id.btSalvar);
		// txtResult = (EditText) findViewById(R.id.idResult);
		// String object = inflater.inflate(R.layout.listitem_attandence,
		// false);

		Intent it = getIntent();
		if (it != null) {
			String params = it.getStringExtra("res");
			if (params != null) {

				varTxtResult = params;

				List<Members> members = Entity.query(Members.class)
						.executeMulti();

				for (int i = 0; i < members.size(); i++) {
					int pos = i + 1;
					if (params.equals("" + members.get(i).getId())) {
						int attendanceQrId = members.get(i).getId();
						Intent intent = new Intent(ResultActivity.this,
								GroupDetailTabsActivity.class);

						intent.putExtra("Member_Attendance", attendanceQrId);
						intent.putExtra("fromResultActivity", "ResultActivity");
						startActivity(intent);

						// imgUnCheck.setVisibility(View.GONE);
						// imgCheck.setSelected(true);

						Toast.makeText(getApplicationContext(),
								"Id Matched Successfully!!", Toast.LENGTH_SHORT)
								.show();
					}

				}

			}
		}
	}

	/**
	 * Save result as file on device
	 */
	public void saveListener(View view) {
		final EditText txtUrl = new EditText(this);

		txtUrl.setHint("");
		new AlertDialog.Builder(this)
				.setTitle(R.string.save_result)
				.setMessage(R.string.save_result_message)
				.setView(txtUrl)
				.setPositiveButton(R.string.save,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								String name = txtUrl.getText().toString();
								if (name.trim().length() > 0) {
									HardwareUtils
											.createFile(varTxtResult, name);
									Toast.makeText(getApplicationContext(),
											R.string.save_result_success,
											Toast.LENGTH_LONG).show();
								}
							}
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {

							}
						}).show();
	}

	/**
	 * Back press button listener
	 */
	@Override
	public void onBackPressed() {
		Functions.callActivity(getApplicationContext(),
				GroupDetailTabsActivity.class, this);
		this.finish();
	}
}
